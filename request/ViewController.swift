//
//  ViewController.swift
//  request
//
//  Created by Shopboostr  on 24/03/16.
//  Copyright © 2016 Shopboostr . All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet var imageView: UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    @IBAction func loadImageButtonTapped(sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
    }

    // Do any additional setup after loading the view, typically from a nib.
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.contentMode = .ScaleAspectFit
            imageView.image = pickedImage
            let imageData = UIImagePNGRepresentation(pickedImage)
            let base64String = imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
            //print(base64String)
            
            let parameters = [
                "image": ["name": "TESTEDWITHIPHONE", "attachment": "data:image/png;base64,\(base64String)"
                ]
            ]
            
            Alamofire.request(.POST, "http://localhost:3000/api/v1/images", parameters: parameters).response { request, response, data, error in
                print(request)
                print(response)
                print(data)
                print(error)
                //print(base64String)
            }
        }
        

        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
        
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Make POST request here
    /*

    */
}


